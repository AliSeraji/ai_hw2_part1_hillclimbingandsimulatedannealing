﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HillClimbing
{
    class AlternativeHillClimbing:SimpleHillClimbing
    {
        public AlternativeHillClimbing(){}

        public Point climbAlternativly(){
            Point p=climbHill();
            for (int i = 0; i < 10000; i++) {
                Point nextP = climbHill();
                if (nextP.Y<p.Y) {
                    p = nextP;
                }
            }
            answer = p;
            return p;
        }
    }
}
