﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OxyPlot;
using OxyPlot.Series;

namespace HillClimbing
{
    class ChartDrawer
    {
        public IPlotView plotView { get; set; }

        public SimpleHillClimbing simpleHillClimbing { get; }
        public AlternativeHillClimbing alternativeHillClimbing { get; }

        public SimulatedAnnealing simulatedAnnealing { get; }

        public ChartDrawer(){
            this.simpleHillClimbing=new SimpleHillClimbing();
            this.alternativeHillClimbing = new AlternativeHillClimbing();
            this.simulatedAnnealing = new SimulatedAnnealing();
        }

        public void drawSimpleHillClimbingAnswer(){
            this.MyHill = new PlotModel { Title = "Hill Climbing" };
            MyHill.InvalidatePlot(true);
            plotView.InvalidatePlot(true);
            this.MyHill.DefaultColors = new List<OxyColor> {
                OxyColors.Blue,
                OxyColors.Red
            };
            this.MyHill.Series.Add(new FunctionSeries(simpleHillClimbing.makeInitialFunctionPoints()
                , simpleHillClimbing.startPoint
                , simpleHillClimbing.endPoint
                , simpleHillClimbing.step
                , "Initial Function"));
            
            Point p = simpleHillClimbing.climbHill();
            var scatterPoint = new ScatterSeries { MarkerType = MarkerType.Circle, MarkerFill = OxyColors.Red };
            scatterPoint.Points.Add(new ScatterPoint(p.X, p.Y, 5, 0));
            this.MyHill.Series.Add(scatterPoint);
           
        }

        public void drawAlternativeHillClimbing(){
            this.MyHill = new PlotModel { Title = "Alternative Hill Climbing" };
            MyHill.InvalidatePlot(true);
            plotView.InvalidatePlot(true);
            this.MyHill.DefaultColors = new List<OxyColor> {
                OxyColors.Blue,
                OxyColors.Red
            };
            this.MyHill.Series.Add(new FunctionSeries(alternativeHillClimbing.makeInitialFunctionPoints()
                , alternativeHillClimbing.startPoint
                , alternativeHillClimbing.endPoint
                , alternativeHillClimbing.step
                , "Initial Function"));

            Point p =alternativeHillClimbing.answer= alternativeHillClimbing.climbAlternativly();
            var scatterPoint = new ScatterSeries { MarkerType = MarkerType.Circle, MarkerFill = OxyColors.Red };
            scatterPoint.Points.Add(new ScatterPoint(p.X, p.Y, 5, 0));
            this.MyHill.Series.Add(scatterPoint);
        }

        public void drawSimulatedAnnealing(){
            this.MyHill = new PlotModel { Title = "Simulated Annealing" };
            MyHill.InvalidatePlot(true);
            plotView.InvalidatePlot(true);
            this.MyHill.DefaultColors = new List<OxyColor> {
                OxyColors.Blue,
                OxyColors.Red
            };
            this.MyHill.Series.Add(new FunctionSeries(simulatedAnnealing.initialFunction
                , simulatedAnnealing.startPoint
                , simulatedAnnealing.endPoint
                , simulatedAnnealing.step
                , "Initial Function"));

            Point p =  simulatedAnnealing.simulateAnneal();
            var scatterPoint = new ScatterSeries { MarkerType = MarkerType.Circle, MarkerFill = OxyColors.Red };
            scatterPoint.Points.Add(new ScatterPoint(p.X, p.Y, 5, 0));
            this.MyHill.Series.Add(scatterPoint);
        }

        public PlotModel MyHill { get; private set; }
        
        
    }
}
