﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HillClimbing
{
    class SimulatedAnnealing{
        public Func<double, double> initialFunction { get; }
        public double step { get; }
        public double startPoint { get; }
        public double endPoint { get; }
        private Random rnd;
        public Point answer { get; private set; }
        public SimulatedAnnealing(){
            initialFunction=(x)=> Math.Sin(10 * Math.PI * x) / (2 * x) + Math.Pow((x - 1), 4);
            step = 0.0001;
            startPoint = 0.5;
            endPoint = 2.5;
            answer = new Point();
            rnd = new Random();
        }

        private Point generateRandomPoint(){
            double rX= rnd.NextDouble() * (endPoint - startPoint) + startPoint;
            return new Point(rX,initialFunction(rX));
        }

        
        public Point simulateAnneal(){
            Point curr = generateRandomPoint();
            double T = 1;
            while(T>1e-10){
                for (int i = 0; i < 10000; i++){
                    Point next = generateRandomPoint();
                    double deltaE = next.Y - curr.Y;
                    double prob = Math.Pow(Math.E, ((-deltaE) / T));
                    double r = rnd.NextDouble() * (1-0)+0;
                    if (deltaE < 0)
                        curr = next;
                    else if (prob > r)
                        curr = next;
                }
                T *= 0.9;
            }
            answer = curr;
            return curr;
        }

        
    }
}
