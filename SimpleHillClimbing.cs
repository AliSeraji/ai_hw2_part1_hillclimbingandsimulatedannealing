﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HillClimbing
{

    class SimpleHillClimbing{
        private Func<double, double> initialFunction;
        public double step { get; }
        public double startPoint { get; }
        public double endPoint { get; }
        public Point answer;
        public SimpleHillClimbing(){
            initialFunction= (x) => Math.Sin(10 * Math.PI * x) / (2 * x) + Math.Pow((x - 1), 4);
            startPoint = 0.5;
            endPoint = 2.5;
            step = 0.0001;
            answer = new Point();
            
        }

        private double generateRandomNum(){
            Random rnd = new System.Random();
            return rnd.NextDouble()*(endPoint-startPoint)+startPoint;
        }

        public Func<double,double> makeInitialFunctionPoints(){
            return initialFunction;
        }

        public Point climbHill(){
            Point curr = new Point();
            curr.X= generateRandomNum();
            curr.Y = initialFunction(curr.X);
            while (true) {
                Point next = getLowestNeighbour(curr);
                if (next.Y > curr.Y){
                    answer = curr;
                    return curr;
                }
                curr = next;
            }
        }

        private Point getLowestNeighbour(Point curr){
            double leftN = initialFunction(curr.X - step);
            double rightN = initialFunction(curr.X + step);
            if (leftN < rightN)
                return new Point(curr.X - step,leftN);
            else
                return new Point(curr.X + step,rightN);
        }
    }
}
