﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HillClimbing
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void HillCimbing_Click(object sender, RoutedEventArgs e)
        {
            
            reslutText.AppendText("Simple HillClimbing...\n");
            ChartDrawer chartDrawer = new ChartDrawer();
            chartDrawer.plotView = MyModel;
            chartDrawer.drawSimpleHillClimbingAnswer();
            MyModel.Model = chartDrawer.MyHill;
            reslutText.AppendText("The answer is::" +chartDrawer.simpleHillClimbing.answer+ "\n");

        }

        private void AlternativeHillCimbing_Click(object sender, RoutedEventArgs e){
            reslutText.AppendText("Alternative HillClimbing...\n");
            ChartDrawer chartDrawer = new ChartDrawer();
            chartDrawer.plotView = MyModel;
            chartDrawer.drawAlternativeHillClimbing();
            MyModel.Model = chartDrawer.MyHill;
            reslutText.AppendText("The answer is::" + chartDrawer.alternativeHillClimbing.answer+ "\n");
        }

        private void SimulatedAnnealing_Click(object sender, RoutedEventArgs e){
            reslutText.AppendText("Simulated Annealing...\n");
            ChartDrawer chartDrawer = new ChartDrawer();
            chartDrawer.plotView = MyModel;
            chartDrawer.drawSimulatedAnnealing();
            MyModel.Model = chartDrawer.MyHill;
            reslutText.AppendText("The answer is::" +chartDrawer.simulatedAnnealing.answer +"\n");
        }
    }
}
